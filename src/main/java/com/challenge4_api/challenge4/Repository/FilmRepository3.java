package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.FilmEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface FilmRepository3 extends JpaRepository<FilmEntity, Long> {
    public Optional<FilmEntity> findBycodeFilm(long codeFilm);
}
