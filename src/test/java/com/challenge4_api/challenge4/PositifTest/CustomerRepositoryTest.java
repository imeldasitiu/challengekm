package com.challenge4_api.challenge4.PositifTest;

import com.challenge4_api.challenge4.Challenge4ApiApplication;
import com.challenge4_api.challenge4.Entity.CustomerEntity;
import com.challenge4_api.challenge4.Repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Challenge4ApiApplication.class)
public class CustomerRepositoryTest {
    @Autowired
    CustomerRepository customerRepository;

    @Test
    public void testFindById(){
        CustomerEntity customer = getCustomer();
        CustomerEntity customerFound = customerRepository.findByIdCustomer(customer.getIdCustomer());
        assertEquals(customer.getIdCustomer(), customerFound.getIdCustomer());
    }

    @Test
    public void testSave(){
        CustomerEntity savetest = getCustomer();
        customerRepository.save(savetest);
        CustomerEntity found = customerRepository.findByIdCustomer(savetest.getIdCustomer());
        assertEquals(savetest.getIdCustomer(), found.getIdCustomer());
    }

    private CustomerEntity getCustomer(){
        CustomerEntity users = new CustomerEntity();
        users.setIdCustomer(1);
        users.setUsername("test");
        users.setPassword("test");
        users.setEmail("test@gmail.com");

        return users;
    }
}
