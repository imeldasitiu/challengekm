package com.challenge4_api.challenge4.Dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class FilmDto {
    long codeFilm;
    String filmName;
    boolean status;
}
