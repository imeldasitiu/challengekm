package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Dto.CustomerOrderDto;
import com.challenge4_api.challenge4.Entity.*;
import com.challenge4_api.challenge4.Repository.CustomerOrderRepository;
import com.challenge4_api.challenge4.Repository.FilmRepository2;
import com.challenge4_api.challenge4.Repository.ScheduleRepository;
import com.challenge4_api.challenge4.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerOrderService {
    @Autowired
    CustomerOrderRepository customerOrderRepository;
    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    FilmRepository2 filmRepository2;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    MasterStudioService masterStudioService;

    public void saveOrder(CustomerOrderDto customerOrderDto){
        CustomerOrderEntity csOrder = new CustomerOrderEntity();
        try {
            ScheduleEntity scheduleEntity = scheduleRepository.findByscheduleId(customerOrderDto.getScheduleId());
            FilmEntity filmEntity= filmRepository2.findBycodeFilm(customerOrderDto.getCodeFilm());
            CustomerEntity usersEntity= customerRepository.findByIdCustomer(customerOrderDto.getIdCustomer().getIdCustomer());

            MasterStudioEntity masterStudioEntity = masterStudioService.updateSeats(customerOrderDto.getIdStudio(), customerOrderDto.isStatus());
            csOrder.setIdStudio(customerOrderDto.getIdStudio());
            csOrder.setCodeFilm(customerOrderDto.getCodeFilm());
            csOrder.setScheduleId(customerOrderDto.getScheduleId());
            csOrder.setIdCustomer(customerOrderDto.getIdCustomer());
            customerOrderRepository.save(csOrder);
        }catch (Exception e){
            System.err.println("Error");
        }
    }
}
