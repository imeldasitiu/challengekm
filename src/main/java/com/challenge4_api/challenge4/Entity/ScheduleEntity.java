package com.challenge4_api.challenge4.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Setter
@Getter
@Table(name = "tbl_schedules")

public class ScheduleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scheduleGenerator")
    @Column(name = "scheduleId")
    private Long scheduleId;

    @Column(name = "tanggal_tayang")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date tanggalTayang;

    @Column(name = "jam_mulai")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamMulai;

    @Column(name = "jam_selesai")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamSelesai;

    @Column(name = "harga_tiket")
    private BigInteger hargaTiket;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "code_film", nullable = false)
    private FilmEntity filmEntity;
}
