package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Services.InvoiceService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;

    @GetMapping("api/report/{format}/{idCustomer}")
    public String exportReport(@PathVariable("format") String format, @PathVariable("idCustomer") Integer idCustomer)throws Exception{
        return invoiceService.reportToPDF(format,idCustomer);
    }

    @GetMapping(value = "api/report2/{format}/{idCustomer}", produces = "application/pdf")
    public byte[] exportReport2(@PathVariable("format") String format, @PathVariable("idUser") Integer idCustomer)throws
            JRException, FileNotFoundException, SQLException {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("parameter1", idCustomer);
        return invoiceService.reportToPDF2(parameters,"Report Invoice");

    }
}
