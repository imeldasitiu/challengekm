package com.challenge4_api.challenge4.NegatifTest;

import com.challenge4_api.challenge4.Challenge4ApiApplication;
import com.challenge4_api.challenge4.Controller.FilmController;
import com.challenge4_api.challenge4.Dto.FilmDto;
import com.challenge4_api.challenge4.Entity.ScheduleEntity;
import com.challenge4_api.challenge4.Repository.ScheduleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Challenge4ApiApplication.class)
public class FilmControllerTest {
    @Autowired
    FilmController filmController;

    @Autowired
    ScheduleRepository scheduleRepository;

    private FilmDto getFilm() {
        FilmDto filmDto = new FilmDto();
        filmDto.setCodeFilm(1);
        filmDto.setFilmName("Dora");
        filmDto.setStatus(true);

        return filmDto;
    }

    @Test
    public void filmSubmitTest(){
        FilmDto filmDto = getFilm();
        ResponseEntity<?> responseEntity = filmController.saveFilm(filmDto);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
    }

    @Test
    public void selectFilmsTest(){
        ResponseEntity<?> responseEntity = filmController.selectAllFilm();
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

    @Test
    public void selectFilmByCodeTest(){
        FilmDto filmDto = getFilm();
        long filmCode = filmDto.getCodeFilm();
        ResponseEntity<?> responseEntity = filmController.selectByCode(filmCode);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

    @Test
    public void selectFilmByStatusTest(){
        FilmDto filmDto = getFilm();
        boolean status = filmDto.isStatus();
        ResponseEntity<?> responseEntity = filmController.selectByStatus(status);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

    @Test
    public void filmUpdateTest(){
        FilmDto filmDto = getFilm();
        long filmCode = filmDto.getCodeFilm();
        ResponseEntity<?> responseEntity = filmController.filmUpdate(filmCode, filmDto);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
    }

    @Test
    public void filmDeleteTest(){
        FilmDto filmDto = getFilm();
        long filmCode = filmDto.getCodeFilm();
        ScheduleEntity schedules = scheduleRepository.findByscheduleId(1);
        if(schedules != null) {
            scheduleRepository.deleteById(filmCode);
            ResponseEntity<?> responseEntity = filmController.filmDelete(filmDto.getCodeFilm());
            assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
        }
    }
}
