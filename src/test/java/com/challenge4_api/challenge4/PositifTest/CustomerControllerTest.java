package com.challenge4_api.challenge4.PositifTest;

import com.challenge4_api.challenge4.Challenge4ApiApplication;
import com.challenge4_api.challenge4.Controller.CustomerController;
import com.challenge4_api.challenge4.Dto.CustomerDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Challenge4ApiApplication.class)
public class CustomerControllerTest {

    @Autowired
    private CustomerController customerController;

    private CustomerDto getCustomer(){
        CustomerDto users = new CustomerDto();
        users.setIdCustomer(1);
        users.setUsername("test");
        users.setPassword("test");
        users.setEmail("test@gmail.com");

        return users;
    }

    @Test
    public void submitCustomerTest(){
        CustomerDto customer = getCustomer();
        ResponseEntity<?> responseEntity = customerController.submitCustomer(customer);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

    @Test
    public void selectCustomersTest(){
        ResponseEntity<?> responseEntity = customerController.selectCustomers();
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
    }

    @Test
    public void selectIdUserTest(){
        CustomerDto customers = getCustomer();
        int customerId = customers.getIdCustomer();
        ResponseEntity<?> responseEntity = customerController.selectByCustomerId(customerId);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);

    }

    @Test
    public void userUpdateTest(){
        CustomerDto update = new CustomerDto();
        int customerId = update.getIdCustomer();
        ResponseEntity<?> responseEntity = customerController.customerUpdate(customerId, update);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

    @Test
    public void userDeleteTest(){
        CustomerDto delete = new CustomerDto();
        int customerId = delete.getIdCustomer();
        ResponseEntity<?> responseEntity = customerController.customerDelete(customerId);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
    }

}
