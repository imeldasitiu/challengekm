package com.challenge4_api.challenge4.Entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "master_studio")
@Setter
@Getter

public class MasterStudioEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id_studio")
    private long idStudio;

    @Column(name ="studio_name")
    private String studioName;

    @Column(name ="no_seats")
    private int noSeats;

    @Column(name ="status")
    private boolean status;
}
