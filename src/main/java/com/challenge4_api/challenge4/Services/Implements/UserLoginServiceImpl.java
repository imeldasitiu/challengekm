package com.challenge4_api.challenge4.Services.Implements;

import com.challenge4_api.challenge4.Entity.UserEntity;
import com.challenge4_api.challenge4.Repository.UserRepository;
import com.challenge4_api.challenge4.Services.UserLoginService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;

@Service
@RequiredArgsConstructor
@Transactional
public class UserLoginServiceImpl implements UserLoginService, UserDetailsService {
    public final UserRepository userRepository;
    public final PasswordEncoder passwordEncoder;

    private final Logger logger = LogManager.getLogger(UserLoginServiceImpl.class);

    @Override
    public UserEntity saveUser(UserEntity user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()+"&*%^$@#23sdasd".getBytes()));
        return userRepository.save(user);
    }

    @Override
    public UserEntity findByusername(String username) {
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username);
        if(userEntity == null ){
            logger.error("user not found");
        }else{
            logger.info(username+ " Found Yeay");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        userEntity.getRoles().forEach(role ->
                authorities.add(new SimpleGrantedAuthority(role.getRole())));
        return new org.springframework.security.core.userdetails.
                User(userEntity.getUsername(), userEntity.getPassword(), authorities);
    }
}
