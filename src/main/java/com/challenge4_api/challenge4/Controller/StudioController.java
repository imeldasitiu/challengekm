package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Dto.StudioDto;
import com.challenge4_api.challenge4.Entity.StudioEntity;
import com.challenge4_api.challenge4.Services.StudioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class StudioController {
    @Autowired
    StudioService studioService;

    @PostMapping("api/studio/submit")
    public Map<String, Object> submitStudio(@RequestBody StudioDto studioDto){
        StudioEntity sSave = studioService.saveStudio(studioDto);

        Map<String, Object> map = new HashMap<>();
        map.put("studio" , studioDto);
        map.put("detail_studio" , studioDto.getDetailStudio());

        return map;
    }
}
