package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Entity.UserEntity;

public interface UserLoginService {
    public UserEntity findByusername(String username);
    public UserEntity saveUser(UserEntity user);
}
