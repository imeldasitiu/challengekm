package com.challenge4_api.challenge4.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "tbl_film")

public class FilmEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generatorFilm")
    @Column(name = "codeFilm")
    private long codeFilm;

    @Column(name = "filmName")
    private String filmName;

    @Column(name = "status")
    private boolean status;

    public FilmEntity(){
    }

}
