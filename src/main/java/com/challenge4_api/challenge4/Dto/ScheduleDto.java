package com.challenge4_api.challenge4.Dto;

import com.challenge4_api.challenge4.Entity.FilmEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;

@Setter
@Getter

public class ScheduleDto {
    long schedule_id;
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date tanggalTayang;
    @JsonFormat(pattern = "HH:mm:ss")
    Date jamMulai;
    @JsonFormat(pattern = "HH:mm:ss")
    Date jamSelesai;
    BigInteger hargaTiket;
    FilmEntity filmEntity;
}
