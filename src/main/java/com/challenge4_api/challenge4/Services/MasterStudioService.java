package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Entity.MasterStudioEntity;
import com.challenge4_api.challenge4.Repository.MasterStudioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MasterStudioService {
    @Autowired
    MasterStudioRepository masterStudioRepository;

    public List<MasterStudioEntity> selectByStatus(boolean status){
        return masterStudioRepository.findByStatus(status);
    }

    public MasterStudioEntity updateSeats(long idStudio, boolean status){
        MasterStudioEntity masterStudio = masterStudioRepository.findByIdStudio(idStudio);
        try {
            if (masterStudio != null){
                masterStudio.setStatus(status);
                return masterStudioRepository.save(masterStudio);
            }
        }catch (Exception e){
            System.out.println("Data Not found");
        }
        return masterStudio;
    }
}
