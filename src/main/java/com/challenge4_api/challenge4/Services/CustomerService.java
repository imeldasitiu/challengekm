package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Dto.CustomerDto;
import com.challenge4_api.challenge4.Entity.CustomerEntity;
import com.challenge4_api.challenge4.Repository.CustomerRepository;
import com.challenge4_api.challenge4.Repository.CustomerRepository2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CustomerRepository2 customerRepository2;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public CustomerEntity saveCustomer(CustomerDto customerDto){
        CustomerEntity uSave = new CustomerEntity();
        uSave.setUsername(customerDto.getUsername());
        uSave.setEmail(customerDto.getEmail());
        uSave.setPassword(customerDto.getPassword());

        return customerRepository.save(uSave);
    }

    public List<CustomerEntity> selectAllCustomer(){
        return customerRepository.findAll();
    }

    public List<CustomerEntity> selectByIdCustomer(int customerId){
        return customerRepository2.findByIdCustomer(customerId);
    }

    public CustomerEntity updateCustomer(int customerId, CustomerDto customerDto){
        CustomerEntity uUpdate = new CustomerEntity();
        try{
            CustomerEntity uEntity = customerRepository.findByIdCustomer(customerId);
            if (uEntity != null){
                uEntity.setUsername(customerDto.getUsername());
                uEntity.setEmail(customerDto.getEmail());
                uEntity.setPassword(customerDto.getPassword());
                uUpdate = customerRepository.save(uEntity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return uUpdate;
    }

    public List<CustomerEntity> deleteCustomer(int customerId){
        CustomerEntity uDelete = customerRepository.findByIdCustomer(customerId);
        if (uDelete != null){
            customerRepository.deleteById(customerId);
        }
        return customerRepository.findAll();
    }
}
