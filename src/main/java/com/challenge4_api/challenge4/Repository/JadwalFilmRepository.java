package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.ViewJadwalFilm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JadwalFilmRepository extends JpaRepository<ViewJadwalFilm, Long> {
    public List<ViewJadwalFilm> findById(long id_jadwal);
    public List<ViewJadwalFilm> findAll();
}
