package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Entity.ViewInvoice;
import com.challenge4_api.challenge4.Repository.InvoiceRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InvoiceService {
    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    RestTemplate restTemplate;

    public String reportToPDF(String format, Integer customer) throws Exception{
        String path = "D:\\Kuliah\\KMSIB - Binar Academy\\challenge4\\pdf";
        List<ViewInvoice> lsVwInvoice = invoiceRepository.findByidCustomer(customer);
        File file = ResourceUtils.getFile("D:\\Kuliah\\KMSIB - Binar Academy\\challenge4\\src\\main\\resources\\Invoice_Tiket.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(lsVwInvoice,false);

        Map<String,Object> paramater = new HashMap<>();
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,paramater,jrBeanCollectionDataSource);

        if (format.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint,path+"\\Invoice_TiketBioskop.pdf");
        }
        return "File have Generated, path : "+path;
    }

    public byte[] reportToPDF2(Map<String, Object> parameters, String reportName){
        try{
            Resource resource = applicationContext.getResource("classpath:Invoice_Tiket.jrxml");
            InputStream inputStream = resource.getInputStream();
            JasperReport report = JasperCompileManager.compileReport(inputStream);

            JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, jdbcTemplate.getDataSource().getConnection());

            byte[] result = JasperExportManager.exportReportToPdf(jasperPrint);

            System.out.println("Done");
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
