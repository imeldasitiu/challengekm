package com.challenge4_api.challenge4.Entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "customer_order")
@Setter
@Getter

public class CustomerOrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tiket")
    private long idTiket;

    @Column(name = "id_studio")
    private long idStudio;

    @Column(name = "schedule_id")
    private long scheduleId;

    @Column(name = "code_film")
    private long codeFilm;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_customer", nullable = false)
    private CustomerEntity idCustomer;
}
