package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Entity.ViewJadwalFilm;
import com.challenge4_api.challenge4.Repository.JadwalFilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JadwalFilmService {
    @Autowired
    JadwalFilmRepository jadwalFilmRepository;

    public List<ViewJadwalFilm> selectViewjadwalFilms(long idJadwal) {
        return jadwalFilmRepository.findById(idJadwal);
    }

    public List<ViewJadwalFilm> selectjadwalFilms(){
        return jadwalFilmRepository.findAll();
    }
}
