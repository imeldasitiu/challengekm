package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Dto.StudioDto;
import com.challenge4_api.challenge4.Entity.MasterStudioEntity;
import com.challenge4_api.challenge4.Entity.StudioEntity;
import com.challenge4_api.challenge4.Repository.MasterStudioRepository;
import com.challenge4_api.challenge4.Repository.StudioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudioService {
    @Autowired
    StudioRepository studioRepository;
    @Autowired
    MasterStudioRepository masterStudioRepository;

    public StudioEntity saveStudio(StudioDto studioDto){
        StudioEntity sSave = new StudioEntity();

        MasterStudioEntity updateSeats = masterStudioRepository.findByIdStudio(studioDto.getDetailStudio().getIdStudio());
        studioDto.getDetailStudio().setStudioName(updateSeats.getStudioName());
        studioDto.getDetailStudio().setNoSeats(updateSeats.getNoSeats());

        sSave.setIdStudio(studioDto.getIdStudio());
        sSave.setStudioName(studioDto.getStudioName());
        sSave.setSeatsNum(studioDto.getSeatsNum());
        return studioRepository.save(sSave);
    }
}
