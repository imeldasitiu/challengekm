package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Entity.ViewJadwalFilm;
import com.challenge4_api.challenge4.Services.JadwalFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class JadwalController {
    @Autowired
    JadwalFilmService jadwalFilmService;

    @GetMapping("api/jadwal-show/{id}")
    public ResponseEntity<?> showJadwal(@PathVariable("id") long idJadwal){
        List<ViewJadwalFilm> show = jadwalFilmService.selectViewjadwalFilms(idJadwal);
        return new ResponseEntity<>(show, HttpStatus.ACCEPTED);
    }

    @PostMapping("api/jadwal-show1/{id}")
    public ResponseEntity<?> showJadwal2(@PathVariable("id") long idJadwal){
        List<ViewJadwalFilm> show = jadwalFilmService.selectjadwalFilms();
        return new ResponseEntity<>(show, HttpStatus.ACCEPTED);
    }
}
