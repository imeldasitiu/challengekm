package com.challenge4_api.challenge4.Entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tbl_studio")
@Setter
@Getter

public class StudioEntity {
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "codeFilm", column = @Column(name = "codeFilm")),
            @AttributeOverride(name = "scheduleId", column = @Column(name = "scheduleId"))
    })
    private StudioPK idStudio;

    @Column(name = "studio_name")
    private String studioName;

    @Column(name = "seats_num")
    private int seatsNum;
}
