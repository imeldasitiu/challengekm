package com.challenge4_api.challenge4.Dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;

@Setter
@Getter

public class JadwalFilmDto {
    private long idJadwal;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date tanggalTayang;
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamMulai;
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamSelesai;
    private String filmName;
    private String studioName;
    private int seatsNum;
    private BigInteger hargaTiket;;
}
