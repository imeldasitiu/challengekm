package com.challenge4_api.challenge4.Dto;

import com.challenge4_api.challenge4.Entity.CustomerEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerOrderDto {
    private long idTiket;
    private long idStudio;
    private long scheduleId;
    private long codeFilm;
    private CustomerEntity idCustomer;
    private  boolean status;
}
