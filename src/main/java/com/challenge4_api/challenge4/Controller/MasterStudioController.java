package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Entity.MasterStudioEntity;
import com.challenge4_api.challenge4.Services.MasterStudioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MasterStudioController {
    @Autowired
    MasterStudioService masterStudioService;

    @GetMapping("api/studio/check/seats/")

    public List<MasterStudioEntity> selectByStatus(@RequestParam(value = "status") boolean status){
        List<MasterStudioEntity> vwStatus = masterStudioService.selectByStatus(status);
        return vwStatus;
    }
}
