package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.FilmEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface FilmRepository extends PagingAndSortingRepository<FilmEntity, Long> {
    public List<FilmEntity> findBycodeFilm(long codeFilm);
    public List<FilmEntity> findAll();
    public List<FilmEntity> findByStatus(Boolean status);
    public Page<FilmEntity> findAll(Pageable pageable);
}
