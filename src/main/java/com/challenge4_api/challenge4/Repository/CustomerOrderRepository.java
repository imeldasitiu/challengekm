package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.CustomerOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CustomerOrderRepository extends JpaRepository<CustomerOrderEntity, Long> {
    public CustomerOrderEntity findByidCustomer(Long idCustomer);
}
