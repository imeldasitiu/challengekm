package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Dto.FilmDto;
import com.challenge4_api.challenge4.Entity.FilmEntity;
import com.challenge4_api.challenge4.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService {
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    FilmRepository2 filmRepository2;


    public FilmEntity filmSave(FilmDto filmDto){
        FilmEntity saveFilm = new FilmEntity();
        saveFilm.setFilmName(filmDto.getFilmName());
        saveFilm.setStatus(filmDto.isStatus());
        return filmRepository.save(saveFilm);
    }

    //panggil cache di redis
    @Cacheable(cacheNames = "cacheFilm")
    public List<FilmEntity> selectAllFilm(){
        return filmRepository.findAll();
    }

    //pagination film
    public Page<FilmEntity> findAll(Pageable pageable){
        return filmRepository.findAll(pageable);
    }

    public List<FilmEntity> selectByCode(long codeFilm){
        return filmRepository.findBycodeFilm(codeFilm);
    }

    public List<FilmEntity> selectByStatus(boolean status){
        status = true;
        return filmRepository.findByStatus(status);
    }

    public FilmEntity updateFilm(long filmCode, FilmDto filmDto){
        FilmEntity fUpdate = new FilmEntity();
        try{
            FilmEntity uFilm = filmRepository2.findBycodeFilm(filmCode);
            if (uFilm != null){
                uFilm.setFilmName(filmDto.getFilmName());
                uFilm.setStatus(filmDto.isStatus());
                fUpdate = filmRepository2.save(uFilm);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return fUpdate;
    }

    public List<FilmEntity> deleteFilm(long filmCode){
        FilmEntity fDelete = filmRepository2.findBycodeFilm(filmCode);
        if (fDelete != null){
            filmRepository2.deleteById(filmCode);
        }
        return filmRepository2.findAll();
    }
}
