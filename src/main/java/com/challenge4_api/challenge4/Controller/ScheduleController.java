package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Dto.ScheduleDto;
import com.challenge4_api.challenge4.Entity.ScheduleEntity;
import com.challenge4_api.challenge4.Services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ScheduleController {
    @Autowired
    ScheduleService scheduleService;

    @PostMapping("api/schedule/submit/{code_film}")
    public ResponseEntity<?> submitSchedules(@PathVariable("code_film") Long codeFilm, @RequestBody ScheduleDto scheduleDto){
        ScheduleEntity save = scheduleService.scheduleSave(codeFilm, scheduleDto);
        if (save != null){
            return new ResponseEntity<>(save, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(save, HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("api/schedule/select-all")
    public ResponseEntity<?> selectSchedules(){
        List<ScheduleEntity> selectAll = scheduleService.selectAllSchedule();
        if (selectAll != null){
            return new ResponseEntity<>(selectAll, HttpStatus.ACCEPTED);
        }else {
            return new ResponseEntity<>(selectAll, HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
