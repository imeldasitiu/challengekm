package com.challenge4_api.challenge4.Services;

import com.challenge4_api.challenge4.Dto.ScheduleDto;
import com.challenge4_api.challenge4.Entity.FilmEntity;
import com.challenge4_api.challenge4.Entity.ScheduleEntity;
import com.challenge4_api.challenge4.Repository.FilmRepository3;
import com.challenge4_api.challenge4.Repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScheduleService {
    @Autowired
    FilmRepository3 filmRepository3;
    @Autowired
    ScheduleRepository scheduleRepository;

    public ScheduleEntity scheduleSave(long codeFilm, ScheduleDto scheduleDto){
        Optional<FilmEntity> fEntity = filmRepository3.findBycodeFilm(codeFilm);
        ScheduleEntity save = new ScheduleEntity();
        save.setTanggalTayang(scheduleDto.getTanggalTayang());
        save.setJamMulai(scheduleDto.getJamMulai());
        save.setJamSelesai(scheduleDto.getJamSelesai());
        save.setHargaTiket(scheduleDto.getHargaTiket());
        save.setFilmEntity(scheduleDto.getFilmEntity());
        return scheduleRepository.save(save);
    }

    public List<ScheduleEntity> selectAllSchedule(){
        return scheduleRepository.findAll();
    }

}
