package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Dto.CustomerDto;
import com.challenge4_api.challenge4.Entity.CustomerEntity;
import com.challenge4_api.challenge4.Services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @PostMapping("api/customer/submit")
    public ResponseEntity<?> submitCustomer(@RequestBody CustomerDto customerDto){
        CustomerEntity save = customerService.saveCustomer(customerDto);
        if (save != null){
            return new ResponseEntity<>(save, HttpStatus.CREATED);
        }else {
            return new ResponseEntity<>(save, HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("api/customer/select-all")
    public ResponseEntity<?> selectCustomers(){
        List<CustomerEntity> select = customerService.selectAllCustomer();
            return new ResponseEntity<>(select, HttpStatus.ACCEPTED);
    }

    @GetMapping("api/customer/select-By-Id/{id_customer}")
    public ResponseEntity<?> selectByCustomerId(@PathVariable("id_customer") int idCustomer){
        List<CustomerEntity> selectId = customerService.selectByIdCustomer(idCustomer);
        if (selectId != null){
            return new ResponseEntity<>(selectId, HttpStatus.ACCEPTED);
        }else {
            return new ResponseEntity<>(selectId, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PutMapping("api/customer/update/{id_customer}")
    public ResponseEntity<?> customerUpdate(@PathVariable ("id_customer") int idCustomer, @RequestBody CustomerDto customerDto){
        try{
            CustomerEntity update = customerService.updateCustomer(idCustomer, customerDto);
            return new ResponseEntity<>(update, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
    }

    @DeleteMapping("api/customer/delete/{id_customer}")
    public ResponseEntity<?> customerDelete(@PathVariable ("id_customer") int idCustomer){
        List<CustomerEntity> delete = customerService.deleteCustomer(idCustomer);
        return new ResponseEntity<>(delete, HttpStatus.ACCEPTED);
    }
}
