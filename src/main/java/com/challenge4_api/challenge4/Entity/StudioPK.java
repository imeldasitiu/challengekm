package com.challenge4_api.challenge4.Entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable

public class StudioPK implements Serializable {
    private long codeFilm;
    private long scheduleId;
}
