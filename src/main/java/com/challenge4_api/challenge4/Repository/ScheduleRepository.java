package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.ScheduleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ScheduleRepository extends JpaRepository<ScheduleEntity, Long> {
    public List<ScheduleEntity> findAll();
    public ScheduleEntity findByscheduleId(long id);
}
