package com.challenge4_api.challenge4.Dto;

import com.challenge4_api.challenge4.Entity.StudioPK;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class StudioDto {
    private StudioPK idStudio;
    private String studioName;
    private int seatsNum;
    private MasterStudioDto detailStudio;
}
