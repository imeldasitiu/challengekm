package com.challenge4_api.challenge4.Dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MasterStudioDto {
    private long idStudio;
    private String studioName;
    private int noSeats;
    private boolean status;
}
