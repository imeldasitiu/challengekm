package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Dto.CustomerOrderDto;
import com.challenge4_api.challenge4.Services.CustomerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerOrderController {
    @Autowired
    CustomerOrderService customerOrderService;

    @PostMapping("api/order/save")
    public ResponseEntity<?> saveOrder(@RequestBody CustomerOrderDto customerOrderDto){
        customerOrderService.saveOrder(customerOrderDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
