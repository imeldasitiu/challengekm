package com.challenge4_api.challenge4.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "view_jadwal_films")
@Setter
@Getter

public class ViewJadwalFilm {
    @Id
    @Column(name = "id_jadwal")
    private long idJadwal;

    @Column(name = "tanggal_tayang")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date tanggalTayang;

    @Column(name = "jam_mulai")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamMulai;

    @Column(name = "jam_selesai")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamSelesai;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "studio_name")
    private String studioName;

    @Column(name = "seats_Num")
    private int seatsNum;

    @Column(name = "harga_tiket")
    private BigInteger hargaTiket;
}
