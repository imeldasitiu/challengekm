package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<CustomerEntity, Integer> {
    public CustomerEntity findByIdCustomer(int idCustomer);
    public List<CustomerEntity> findAll();
}
