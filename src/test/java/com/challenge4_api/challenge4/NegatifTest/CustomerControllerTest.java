package com.challenge4_api.challenge4.NegatifTest;

import com.challenge4_api.challenge4.Challenge4ApiApplication;
import com.challenge4_api.challenge4.Controller.CustomerController;
import com.challenge4_api.challenge4.Dto.CustomerDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Challenge4ApiApplication.class)
public class CustomerControllerTest {

    @Autowired
    private CustomerController customerController;

    private CustomerDto getCustomer(){
        CustomerDto customer = new CustomerDto();
        customer.setIdCustomer(1);
        customer.setUsername("test");
        customer.setPassword("test");
        customer.setEmail("test@gmail.com");

        return customer;
    }

    @Test
    public void submitCustomerTest(){
        CustomerDto customer = getCustomer();
        ResponseEntity<?> responseEntity = customerController.submitCustomer(customer);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
    }

    @Test
    public void selectCustomersTest(){
        ResponseEntity<?> responseEntity = customerController.selectCustomers();
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

    @Test
    public void selectIdCustomerTest(){
        CustomerDto customerId = getCustomer();
        int idCustomer = customerId.getIdCustomer();
        ResponseEntity<?> responseEntity = customerController.selectByCustomerId(idCustomer);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);

    }

    @Test
    public void customerUpdateTest(){
        CustomerDto update = new CustomerDto();
        int idCustomer = update.getIdCustomer();
        ResponseEntity<?> responseEntity = customerController.customerUpdate(idCustomer, update);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(202);
    }

    @Test
    public void customerDeleteTest(){
        CustomerDto delete = new CustomerDto();
        int idCustomer = delete.getIdCustomer();
        ResponseEntity<?> responseEntity = customerController.customerDelete(idCustomer);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
    }

}
