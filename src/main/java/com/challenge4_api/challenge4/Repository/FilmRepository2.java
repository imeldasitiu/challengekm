package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.FilmEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface FilmRepository2 extends JpaRepository<FilmEntity, Long> {
    public FilmEntity findBycodeFilm(long codeFilm);
}
