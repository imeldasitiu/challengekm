package com.challenge4_api.challenge4.NegatifTest;

import com.challenge4_api.challenge4.Repository.CustomerRepository;
import com.challenge4_api.challenge4.Services.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomerRepositoryTest {

    @Mock
    private CustomerRepository customerRepository;
    private CustomerService customerService;

    @BeforeEach
    void setUp(){
        this.customerService = new CustomerService(this.customerRepository);
    }

    @Test
    void getAllPerson(){
        customerService.selectAllCustomer();
        Mockito.verify(customerRepository).findAll();
    }

}
