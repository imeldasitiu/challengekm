package com.challenge4_api.challenge4.PositifTest;

import com.challenge4_api.challenge4.Challenge4ApiApplication;
import com.challenge4_api.challenge4.Challenge4ApiApplicationTests;
import com.challenge4_api.challenge4.Entity.FilmEntity;
import com.challenge4_api.challenge4.Repository.FilmRepository2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Challenge4ApiApplication.class)

public class FilmRepositoryTest {
    @Autowired
    FilmRepository2 filmRepository;

    @Test
    public void testFindById(){
        FilmEntity film = getFilm();
        FilmEntity userFound = filmRepository.findBycodeFilm(film.getCodeFilm());
        assertEquals(film.getCodeFilm(), userFound.getCodeFilm());
    }

    @Test
    public void testSave(){
        FilmEntity savetest = getFilm();
        filmRepository.save(savetest);
        FilmEntity found = filmRepository.findBycodeFilm(savetest.getCodeFilm());
        assertEquals(savetest.getCodeFilm(), found.getCodeFilm());
    }

    @Test
    private FilmEntity getFilm(){
        FilmEntity film = new FilmEntity();
        film.setCodeFilm(1);
        film.setFilmName("hulk");
        film.setStatus(true);

        return film;
    }
}
