package com.challenge4_api.challenge4.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Setter
@Getter
@Table(name = "view_invoice")

public class ViewInvoice {
    @Id
    @Column(name = "id_customer")
    private int idCustomer;

    @Column(name = "id_tiket")
    private long idTiket;

    @Column(name = "harga_tiket")
    private BigDecimal hargaTiket;

    @Column(name = "jam_mulai")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamMulai;

    @Column(name = "jam_selesai")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date jamSelesai;

    @Column(name = "username")
    private String username;

    @Column(name = "no_seats")
    private int noSeats;

    @Column(name = "studio_name")
    private String studioName;

    @Column(name = "film_name")
    private String filmName;
}
