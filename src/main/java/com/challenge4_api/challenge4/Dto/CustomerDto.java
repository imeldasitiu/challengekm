package com.challenge4_api.challenge4.Dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class CustomerDto {
    int idCustomer;
    String username;
    String email;
    String password;
}
