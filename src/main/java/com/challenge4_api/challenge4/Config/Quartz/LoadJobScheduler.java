package com.challenge4_api.challenge4.Config.Quartz;

import com.challenge4_api.challenge4.Services.FilmService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class LoadJobScheduler extends QuartzJobBean {
    FilmService filmService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        filmService.selectAllFilm();
    }
}

