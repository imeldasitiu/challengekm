package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.StudioPK;
import com.challenge4_api.challenge4.Entity.StudioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface StudioRepository extends JpaRepository<StudioEntity, StudioPK> {
}
