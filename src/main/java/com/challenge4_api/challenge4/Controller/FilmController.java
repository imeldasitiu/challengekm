package com.challenge4_api.challenge4.Controller;

import com.challenge4_api.challenge4.Dto.FilmDto;
import com.challenge4_api.challenge4.Entity.FilmEntity;
import com.challenge4_api.challenge4.Services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FilmController {

    @Autowired
    FilmService filmService;

    @PostMapping("api/film/submit")
    public ResponseEntity<?> saveFilm(@RequestBody FilmDto filmDto){
        FilmEntity save = filmService.filmSave(filmDto);
        if (save != null){
            return new ResponseEntity<>(save, HttpStatus.CREATED);
        }else {
            return new ResponseEntity<>(save, HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("api/film/select-all")
    public ResponseEntity<?> selectAllFilm(){
        List<FilmEntity> select = filmService.selectAllFilm();
        if (select != null){
            return new ResponseEntity<>(select, HttpStatus.ACCEPTED);
        }else {
            return new ResponseEntity<>(select, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    //pagination film
    @GetMapping ("api/film/select-all/{page}/{size}")
    public Map<String, Object> filmListPaging(Pageable pageable, @PathVariable("page")int page, @PathVariable("size") int size){
        pageable = PageRequest.of(page, size);
        Page<FilmEntity> films = filmService.findAll(pageable);
        Map <String, Object> map = new HashMap<>();
        map.put("page", page);
        map.put("size", size);
        map.put("data", films.getContent());
        map.put("row count", films.getTotalElements());
        return map;
    }

    @GetMapping("api/film/select-by-code/{code}")
    public ResponseEntity<?> selectByCode(@PathVariable("code") long codeFilm){
        List<FilmEntity> selectId = filmService.selectByCode(codeFilm);
        if (selectId != null){
            return new ResponseEntity<>(selectId, HttpStatus.ACCEPTED);
        }else {
            return new ResponseEntity<>(selectId, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @GetMapping("api/film/select-by-status")
    public ResponseEntity<?> selectByStatus(boolean status){
        List<FilmEntity> selectStatus = filmService.selectByStatus(status);
        if (selectStatus != null){
            return new ResponseEntity<>(selectStatus, HttpStatus.ACCEPTED);
        }else {
            return new ResponseEntity<>(selectStatus, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PutMapping("api/film/update/{code}")
    public ResponseEntity<?> filmUpdate(@PathVariable ("code") long codeFilm, @RequestBody FilmDto filmDto){
        try{
            FilmEntity update = filmService.updateFilm(codeFilm, filmDto);
            return new ResponseEntity<>(update, HttpStatus.CREATED);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
    }

    @DeleteMapping("api/film/delete/{code}")
    public ResponseEntity<?> filmDelete(@PathVariable ("code") long codeFilm){
        List<FilmEntity> delete = filmService.deleteFilm(codeFilm);
        return new ResponseEntity<>(delete, HttpStatus.ACCEPTED);
    }

}
