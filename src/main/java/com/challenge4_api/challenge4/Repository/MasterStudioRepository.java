package com.challenge4_api.challenge4.Repository;

import com.challenge4_api.challenge4.Entity.MasterStudioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface MasterStudioRepository extends JpaRepository<MasterStudioEntity, Long> {
    public List<MasterStudioEntity> findByStatus(boolean status);
    public MasterStudioEntity findByIdStudio(long idStudio); //
}
